﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEditor;


public class EnemyBehaviour : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;
    private Animator anim;
    private bool walking;
    private bool fighting;
    public float lookRadius;
    public float attackRadius;
    private float nextAttack;
    public float attackRate = 1f;
    Transform targetPlayer;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        targetPlayer = PlayerManager.instance.ourPlayer.transform;
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("walking", walking);
        anim.SetBool("fighting", fighting);

        if (!walking)
        {
            anim.SetBool("idling", true);
        }
        else
        {
            anim.SetBool("idling", false);
        }

        float distance = Vector3.Distance(transform.position, targetPlayer.position);
        if(distance <= lookRadius)
        {
            MoveAndAttack();
        }
        else
        {
            walking = false;
        }
    }

    void MoveAndAttack()
    {
        navMeshAgent.destination = targetPlayer.position;
        if(!navMeshAgent.pathPending && navMeshAgent.remainingDistance > attackRadius)
        {
            navMeshAgent.isStopped = false;
            walking = true;
        }
        else if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= attackRadius)
        {
            //anim.SetBool("fighting", false);
            Vector3 targetPostition = new Vector3(targetPlayer.position.x, this.transform.position.y, targetPlayer.position.z);
            this.transform.LookAt(targetPostition);
            if (Time.time > nextAttack)
            {
                nextAttack = Time.time + attackRate;
                anim.SetBool("fighting", true);
            }
            navMeshAgent.isStopped = true;
            walking = false;
        }
    }
}
