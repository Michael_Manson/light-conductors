﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CannonController : MonoBehaviour
{
    private NavMeshAgent navMeshAgent;

    public float lookRadius;
    public float attackRadius;

    private float nextAttack;
    public float attackRate = 2f;

    Transform targetPlayer;
    public Transform target;
    public GameObject Bullet_Emitter;
    public GameObject Bullet;
    public float Bullet_Forward_Force;
    public AudioClip cannon;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        targetPlayer = PlayerManager.instance.ourPlayer.transform;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Vector3.Distance(transform.position, targetPlayer.position);
        if (distance <= lookRadius)
        {
            Vector3 targetPostition = new Vector3(target.position.x, this.transform.position.y, target.position.z);
            this.transform.LookAt(targetPostition);

            if (navMeshAgent.remainingDistance <= attackRadius)
            {
                if (Time.time > nextAttack)
                {
                    nextAttack = Time.time + attackRate;
                    audioSource.PlayOneShot(cannon);
                    GameObject Temporary_Bullet_Handler;
                    Temporary_Bullet_Handler = Instantiate(Bullet, Bullet_Emitter.transform.position, Bullet_Emitter.transform.rotation) as GameObject;
                    Temporary_Bullet_Handler.transform.Rotate(Vector3.left * 90);
                    Rigidbody Temporary_RigidBody;
                    Temporary_RigidBody = Temporary_Bullet_Handler.GetComponent<Rigidbody>();
                    Temporary_RigidBody.AddForce(transform.forward * Bullet_Forward_Force);
                    Destroy(Temporary_Bullet_Handler, 1.0f);
                }
            }
        }

    }
}
