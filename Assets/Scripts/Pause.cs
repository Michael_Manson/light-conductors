﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameManager gm;
    private GameManager.GameState lastGameState;

    [SerializeField] private GameObject pausePanel;

    // Update is called once per frame
    void Update()
    {
        InputHandle();
    }

    private void InputHandle()
    {
        if (Input.GetButtonDown("Pause"))
        {
            if (gm.currentGameState != GameManager.GameState.Paused)
            {
                PauseGame();
                pausePanel.SetActive(true);
            }
            else
            {
                gm.currentGameState = lastGameState;
                pausePanel.SetActive(false);
                Time.timeScale = 1;
            }
        }
    }

    public void ResumeGame()
    {
        gm.currentGameState = lastGameState;
        pausePanel.SetActive(false);
        Time.timeScale = 1;
    }

    public void PauseGame()
    {
        lastGameState = gm.currentGameState;
        gm.currentGameState = GameManager.GameState.Paused;
        Time.timeScale = 0;
        Debug.Log("pause");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void Feedback()
    {
        Application.OpenURL("https://forms.gle/kqiBUVQEpujSLsQU6");
    }
}
