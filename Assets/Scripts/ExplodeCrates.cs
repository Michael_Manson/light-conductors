﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeCrates : MonoBehaviour
{
    [SerializeField] private GameObject explode;
    [SerializeField] private GameObject crate1;
     [SerializeField] private GameObject crate2;
     [SerializeField] private GameObject crate3;
     [SerializeField] private GameObject crate4;
    AudioSource audioSource;
    public AudioClip boom;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            audioSource.PlayOneShot(boom);
            Vector3 offset = new Vector3(-10f, 2f, 0);
            Destroy(Instantiate(explode, this.transform.position + offset, Quaternion.identity), 2.0f);
            Destroy(crate1);
            Destroy(crate2);
            Destroy(crate3);
            Destroy(crate4);
            GetComponent<Collider>().enabled = false;
            this.enabled = false;
        }
    }
}
