﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraineeBehaviour : MonoBehaviour
{
    bool playAnim = true;
    public Animator anim;
    [SerializeField] private AudioClip taunt;
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    public IEnumerator WaitAnim()
    {
        playAnim = false;
        anim.SetBool("isPointing", playAnim);
        float randomWait = Random.Range(5, 8);       
        yield return new WaitForSeconds(randomWait);
        playAnim = true;
        anim.SetBool("isPointing", playAnim);
        audioSource.PlayOneShot(taunt);

    }

    // Update is called once per frame
    void Update()
    {
        if (playAnim == true)
            StartCoroutine(WaitAnim()); //wait random seconds for animation
    }
}
