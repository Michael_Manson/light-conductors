﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomObjectScroll : MonoBehaviour
{
    public float speed;
    public GameObject prefab;

    void Update()
    {
        if (transform.position.x < 320)
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
        }
        else
        {
        //Destroy(gameObject);
        Vector3 position = new Vector3(Random.Range(-410f, -480f), 3.5768f, Random.Range(-5.9f, 30.7f));
        transform.position = position;
        transform.rotation = Quaternion.Euler(Random.Range(-10, 10), 0, 0);
        }
    }

}
