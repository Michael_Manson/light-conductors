﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ClickToMove : MonoBehaviour
{
    [Header("Stats")]
    public float attackDistance;
    public float attackRate;
    private float nextAttack;

    private NavMeshAgent navMeshAgent;
    private Animator anim;

    private Transform targetedEnemy;
    private bool enemyClicked;
    private bool walking;

    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //float horInput = Input.GetAxis("Horizontal");
        //float verInput = Input.GetAxis("Vertical");
        //Vector3 movement = new Vector3(horInput, 0f, verInput);
       //Vector3 moveDestination = transform.position + movement;
        //GetComponent<NavMeshAgent>().destination = moveDestination;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetButtonDown("Fire2"))
        {
            navMeshAgent.ResetPath();
            if (Physics.Raycast(ray, out RaycastHit hit, 1000))
            {
                if (hit.collider.CompareTag("Enemy"))
                {
                    targetedEnemy = hit.transform;
                    enemyClicked = true;

                }
                else
                {
                    walking = true;
                    enemyClicked = false;
                    navMeshAgent.isStopped = false;
                    navMeshAgent.destination = hit.point;
                }
            }
        }
        if (enemyClicked)
        {
            MoveAndAttack();
        }
        else
        {
            if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
            {
                walking = false;
            }
            else if(!navMeshAgent.pathPending && navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
            {
                walking = true;
            }
        }
          
        anim.SetBool("isWalking", walking);
    }

    void MoveAndAttack()
    {
        if (targetedEnemy == null)
        {
            return;
        }
        navMeshAgent.destination = targetedEnemy.position;

        if(!navMeshAgent.pathPending && navMeshAgent.remainingDistance > attackDistance)
        {
            navMeshAgent.isStopped = false;
            walking = true;
        }
        else if(!navMeshAgent.pathPending && navMeshAgent.remainingDistance <= attackDistance)
        {
            anim.SetBool("isAttacking", false);
            transform.LookAt(targetedEnemy);
            Vector3 dirToAttack = targetedEnemy.transform.position - transform.position;

            if(Time.time > nextAttack)
            {
                nextAttack = Time.time + attackRate;
                anim.SetBool("isAttacking", true);
            }
            navMeshAgent.isStopped = true;
            walking = false;
        }
    }
}
