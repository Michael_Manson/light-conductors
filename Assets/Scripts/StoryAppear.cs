﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryAppear : MonoBehaviour
{
    [SerializeField] private GameObject storyPanel;
    public Pause pauseGame;
    private void Start()
    {
        storyPanel.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            storyPanel.SetActive(true);
            pauseGame.PauseGame();
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            storyPanel.SetActive(false);
            Destroy(this);
        }

    }
}
