﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrackeyMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    Vector3 velocity;
    bool isGrounded;
    public float gravity = -9.81f;
    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;
    public float jumpHeight = 3f;
    public Animator anim;
    private bool walking;
    private bool walkingBack;
    private bool jumping;
    private bool useLight;
    private bool canUseLight;
    public Camera mainCamera;
    public ParticleSystem jetpack;
    public AudioClip stomp;
    AudioSource audioSource;
    public AudioClip zap;
    public AudioClip jetBlast;
    public GameObject lightBomb;
    public GameObject image;
    public GameObject lightLock;
    [SerializeField] private GameManager gm;

    void Start()
    {
        anim = GetComponent<Animator>();
        //anim = GetComponentInChildren<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("kneel"))
        {
            return;
        }
        else
        {
            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -21f;
            }

            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = transform.right * x + transform.forward * z;

            if (z > 0 && jumping == false)
            {
                walking = true;
                if (isGrounded == true && audioSource.isPlaying == false)
                {
                    PlayStomp();
                }
            }
            else
            {
                walking = false;
            }

            anim.SetBool("isWalking", walking);

            if (z < 0)
            {
                walkingBack = true;
            }
            else
            {
                walkingBack = false;
            }
            anim.SetBool("isBackingUp", walkingBack);

            controller.Move(move * speed * Time.deltaTime);

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
                audioSource.PlayOneShot(jetBlast);
                Destroy(Instantiate(jetpack, transform), 1.0f);
                jumping = true;
            }
            else
            {
                jumping = false;
            }
            anim.SetBool("isJumping", jumping);

            velocity.y += gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
            
            if (Input.GetKeyDown(KeyCode.E) && canUseLight==true)
                {
                useLight = true;
                Invoke("CreateLight", 5);
                audioSource.PlayOneShot(zap);
                Destroy(lightLock, 5.0f);
                image.SetActive(false);
            }
                else
                {
                    useLight = false;
                }
            }

            anim.SetBool("useLight", useLight);
        }


    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("LightUse"))
        {
            canUseLight = true;
            image.SetActive(true);
            lightLock = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("LightUse"))
        {
            canUseLight = false;
            image.SetActive(false);
        }
    }

    void PlayStomp()
    {
        if (gm.currentGameState != GameManager.GameState.Paused)
        {
            audioSource.volume = Random.Range(0.8f, 1);
            audioSource.pitch = Random.Range(0.8f, 1.1f);
            audioSource.Play();
        }
    }

    void CreateLight()
    {
        Vector3 offset = new Vector3(-2f, 1f, 0);
        Destroy(Instantiate(lightBomb, this.transform.position +offset, Quaternion.identity), 2.0f);
    }
}
