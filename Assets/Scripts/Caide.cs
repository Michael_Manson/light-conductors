﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Caide : MonoBehaviour
{
    public float maxHealth = 100;
    float currentHealth;
    public GameObject healthBarUI;
    public Slider slider;
    public AudioClip impact;
    public AudioClip die;
    AudioSource audioSource;
    public ParticleSystem hitSpark;

    public Animator animator;

    [SerializeField] private GameObject storyPanel;
    public Pause pauseGame;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        slider.value = CalculateHealth();
        audioSource = GetComponent<AudioSource>();
        storyPanel.SetActive(false);
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        audioSource.PlayOneShot(impact);
        slider.value = CalculateHealth();
        healthBarUI.SetActive(true);
        Vector3 offset = new Vector3(0, 1f, 0);
        Destroy(Instantiate(hitSpark, this.transform.position + offset, Quaternion.identity), 1.0f);
        animator.SetTrigger("Hurt");

        

        if (currentHealth <= 0)
        {
            storyPanel.SetActive(true);
            pauseGame.PauseGame();
            Die();
        }
    }

    void Die()
    {
        animator.SetBool("isDead", true);
        healthBarUI.SetActive(false);
        GetComponent<Collider>().enabled = false;
        this.enabled = false;
        GetComponent<EnemyBehaviour>().enabled = false;
        Destroy(gameObject);
    }

    float CalculateHealth()
    {
        return currentHealth / maxHealth;

    }
}
