﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingTrack : MonoBehaviour
{
    public float speed;

    void Update()
    { if (transform.position.x < 851)
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
        }
        else transform.position = new Vector3(-410.2f, -7.229384f, 44.01f);
            
    }

}
