﻿using UnityEngine;
using System.Collections;


public class ScrollerTest : MonoBehaviour
{
    //[SerializeField] private float scrollPos;
    [SerializeField] private float scrollSpeed;

    void FixedUpdate()
    {
        transform.position = new Vector3(Screen.width * 0.5f, transform.position.y + scrollSpeed, 0);
     
    }
}