﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public float maxHealth = 100;
    float currentHealth;
    public GameObject healthBarUI;
    public Slider slider;
    public AudioClip impact;
    public AudioClip die;
    AudioSource audioSource;
    public ParticleSystem hitSpark;

    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        currentHealth = maxHealth;
        audioSource = GetComponent<AudioSource>();
        slider.value = CalculateHealth();
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        slider.value = CalculateHealth();
        healthBarUI.SetActive(true);
        Debug.Log(slider.value);
        audioSource.PlayOneShot(impact);
            
        Vector3 offset = new Vector3(0, 1f, 0);
        Destroy(Instantiate(hitSpark, this.transform.position + offset, Quaternion.identity), 1.0f);
        animator.SetTrigger("Hurt");

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Debug.Log("Enemy Died!");
        animator.SetBool("isDead", true);
        audioSource.PlayOneShot(die);
        healthBarUI.SetActive(false);
        GetComponent<Collider>().enabled = false;
        this.enabled = false;
        GetComponent<EnemyBehaviour>().enabled = false;
    }
   
    float CalculateHealth()
    {
        return currentHealth / maxHealth;
        
    }
}
