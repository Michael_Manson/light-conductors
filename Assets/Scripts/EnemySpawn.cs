﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject prefab; // Drag and drop prefab to component in unity
    [SerializeField] private int spawnNumber;

    // When trigger is triggered
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            for (int i = 0; i < spawnNumber; i++)
            {
                Instantiate(prefab, new Vector3(transform.position.x - 35f, transform.position.y, transform.position.z), Quaternion.identity);
            }
            GetComponent<Collider>().enabled = false;
            this.enabled = false;
        }
    }
}
